using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        private Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
        private bool isStop = false;
        private bool isPaused = false;
        private bool crouch;

        [SerializeField]
        private GameObject[] pixelsGroup;
        private List<Vector3> originalPixelsPosition = new List<Vector3>();

        [SerializeField]
        private GameObject characterMesh;

        public bool Crouch
        {
            get { return crouch; }
        }

        public Material CharacterMaterial;
        
        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();

            foreach(GameObject g in pixelsGroup)
            {
                Vector3 pixelPosition = new Vector3();
                pixelPosition.x = g.transform.localPosition.x;
                pixelPosition.y = g.transform.localPosition.y;
                pixelPosition.z = g.transform.localPosition.z;
                originalPixelsPosition.Add(pixelPosition);
                g.SetActive(false);
            }
        }

        public void PauseToggle(bool isPausedInvoked = false)
        {
            isPaused = isPausedInvoked;
        }

        public void Dead()
        {
            isStop = true;

            characterMesh.SetActive(false);

            //CharacterMaterial.SetFloat("_Mode", 3);
            //CharacterMaterial.SetColor("_Color", new Color(1, 1, 1, 0.0f));
            m_Move = Vector3.zero;
            foreach (GameObject g in pixelsGroup)
            {
                g.SetActive(true);
                g.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

        public void Restart()
        {
            for (int i = 0; i < pixelsGroup.Length; i++)
            {
                pixelsGroup[i].GetComponent<Rigidbody>().isKinematic = true;
                pixelsGroup[i].transform.localPosition = 
                    new Vector3(originalPixelsPosition[i].x,
                                originalPixelsPosition[i].y,originalPixelsPosition[i].z);
                pixelsGroup[i].SetActive(false);
            }

            characterMesh.SetActive(true);

            isStop = false;
            //CharacterMaterial.SetFloat("_Mode", 0);
            //CharacterMaterial.SetColor("_Color", new Color(1, 1, 1, 1.0f));
        }

        private void Update()
        {
            if(isStop)
            {
                return;
            }

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            if(Input.GetKeyDown("joystick button 17"))
            {
                //crouch = Input.GetKey("joystick button 14");
            crouch = crouch == true ? false : true;
            }
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (Input.GetKeyDown("joystick button 1"))
            {
                //crouch = Input.GetKey("joystick button 14");
                crouch = crouch == true ? false : true;
            }
#endif

            if(Input.GetKeyDown(KeyCode.LeftControl))
            {
                crouch = crouch == true ? false : true;
            }

            //if (!m_Jump)
            //{
            //    m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            //}
        }


        // Fixed update is called in sync with physics
        private void FixedUpdate()
        {
            if(isStop || isPaused)
            {
                m_Move = Vector3.zero;
                m_Character.Move(m_Move, false, m_Jump);
                return;
            }

            // read inputs
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");
            //crouch = Input.GetKey(KeyCode.C);

            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v*m_CamForward + h*m_Cam.right;
            }
            else
            {
                // we use world-relative directions in the case of no main camera
                m_Move = v*Vector3.forward + h*Vector3.right;
            }
#if !MOBILE_INPUT
			// walk speed multiplier
	        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

            // pass all parameters to the character control script
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }
    }
}
