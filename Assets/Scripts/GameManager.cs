﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Rewind.Core.Playback;
using Rewind.Core.Character;
using Rewind.GUI;
using Rewind.Core.SpawnPoint;
using Rewind.Core;
using UnityStandardAssets.Characters.ThirdPerson;
using Rewind.Core.Environment;

namespace Rewind.Core.Manager
{
    [Serializable]
    public class Progression
    {
        public Goal[] ProgressionGoals;
    }

    public class GameManager : MonoBehaviour
    {
        private static GameManager instance;
        public static GameManager Instance
        {
            get { return instance; }
        }

        //public GameObject PlayerPrefab;
        public GameObject TargetPlayer;
        public GameObject GhostPrefab;
        public Guard[] Guards;
        public GameObject[] spawnPoints;
        public GameObject[] goals;
        public Progression[] Progress;
        public GameObject[] StartPoints;
        public GameObject ContinueTextUI;
        public GameObject RestartTextUI;
        public float RaycastRange = 100f;
        public float PlaybackAccuracy = .1f;
        public DialogManager dialogManager;
        public Alarm alarm;
        public GameObject almanacObject;
        public GameObject MainScreen;

        List<Record> playbackRecords;
        List<Ghost> ghosts;
        bool isReplay = false;
        bool isRestart = false;
        bool isGameStarted = false;
        int currentSpawnPointId = 0;

        GameObject spawnPoint;
        GameObject goal;
        //NavMeshAgent playerAgent;
        Vector3 previousPosition;
        Vector3 previousVelocity;
        float startTime;
        int currentGoalId;
        bool isAlarmTriggered = false;
        bool isGamePaused = true;

        public int CurrentGoalId
        {
            get { return currentGoalId; }
        }

        int progressLevel = 0;
        Progression currentProgress;

        ThirdPersonUserControl thirdPersonUserControl;

		private void Awake()
		{
            instance = this;
		}

		private void Start()
		{
            thirdPersonUserControl = TargetPlayer.GetComponent<ThirdPersonUserControl>();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
		}

        public void BeginGameSequence()
        {
            StartGame();
            thirdPersonUserControl.PauseToggle(true);
            UICanvas.Instance.ShowWelcomeTutorial(() =>
            {
                thirdPersonUserControl.PauseToggle();
            });
        }

        void StartGame()
        {
            almanacObject.SetActive(true);
            UICanvas.Instance.ShowAlmanacToggle();

            UICanvas.Instance.ResetIndicator();

            UICanvas.Instance.NarrativePanel.ResetNarrativePanel();

            ResetGoals();

            GetNextGoal();

            foreach(Guard g in Guards)
            {
                g.Reposition();
            }

            alarm.TurnOff();

            if (StartPoints != null)
            {
                foreach (GameObject sp in StartPoints)
                {
                    sp.SetActive(false);
                }

                StartPoints[progressLevel].SetActive(true);
                StartPoints[progressLevel].GetComponent<StartPoint>().ShowWorldCanvas();
            }

            spawnPoint = spawnPoints[currentSpawnPointId];

            Vector3 spawnPointPosition = spawnPoint.transform.position;

            TargetPlayer.transform.position = spawnPointPosition;

            playbackRecords = Recorder.Instance.StartRecorder(playbackRecords);

            PlaybackEvent playbackEvent = new PlaybackEvent();
            playbackEvent.PositionX = spawnPointPosition.x;
            playbackEvent.PositionY = spawnPointPosition.y;
            playbackEvent.PositionZ = spawnPointPosition.z;

            Recorder.Instance.RecordEvent(playbackEvent);

            if (playbackRecords != null && playbackRecords.Count > 0)
            {
                if(ghosts!=null)
                {
                    foreach (Ghost ghost in ghosts)
                    {
                        Destroy(ghost.gameObject);
                    }

                    ghosts.Clear();
                }

                for (int i = 0; i < playbackRecords.Count; i++)
                {
                    GameObject ghostObject = Instantiate(GhostPrefab, new Vector3(250, 1, 250), Quaternion.identity);
                    if(ghosts==null)
                    {
                        ghosts = new List<Ghost>();
                    }
                    Ghost g = ghostObject.GetComponent<Ghost>();
                    g.InitialiseGhost(playbackRecords[i],i.ToString(),TargetPlayer.transform);
                    ghosts.Add(g);
                }
            }

            dialogManager.ResetDialog();

            startTime = Time.time;

            isGameStarted = true;
        }

        void RestartGame()
        {
            almanacObject.SetActive(true);
            UICanvas.Instance.ShowAlmanacToggle();

            UICanvas.Instance.ResetIndicator();

            UICanvas.Instance.NarrativePanel.ResetNarrativePanel();

            RestartGoal();

            if(StartPoints != null)
            {
                foreach(GameObject sp in StartPoints)
                {
                    sp.SetActive(false);
                }

                StartPoints[progressLevel].SetActive(true);
                StartPoints[progressLevel].GetComponent<StartPoint>().ShowWorldCanvasRestart();
            }

            foreach (Guard g in Guards)
            {
                g.Reposition();
            }

            alarm.TurnOff();
            //isAlarmTriggered = false;

            spawnPoint = spawnPoints[currentSpawnPointId];

            Vector3 spawnPointPosition = spawnPoint.transform.position;

            TargetPlayer.transform.position = spawnPointPosition;
            thirdPersonUserControl.Restart();

            Recorder.Instance.RestartRecorder();

            PlaybackEvent playbackEvent = new PlaybackEvent();
            playbackEvent.PositionX = spawnPointPosition.x;
            playbackEvent.PositionY = spawnPointPosition.y;
            playbackEvent.PositionZ = spawnPointPosition.z;

            Recorder.Instance.RecordEvent(playbackEvent);

            if (playbackRecords != null && playbackRecords.Count > 0)
            {
                if (ghosts != null)
                {
                    foreach (Ghost ghost in ghosts)
                    {
                        Destroy(ghost.gameObject);
                    }

                    ghosts.Clear();
                }

                for (int i = 0; i < playbackRecords.Count; i++)
                {
                    GameObject ghostObject = Instantiate(GhostPrefab, new Vector3(250, 1, 250), Quaternion.identity);
                    if (ghosts == null)
                    {
                        ghosts = new List<Ghost>();
                    }
                    Ghost g = ghostObject.GetComponent<Ghost>();
                    g.InitialiseGhost(playbackRecords[i], i.ToString(), TargetPlayer.transform);
                    ghosts.Add(g);
                }
            }

            dialogManager.ResetDialog();

            startTime = Time.time;

            isGameStarted = true;
        }

        public void PauseGame()
        {
            isGamePaused = true;
            thirdPersonUserControl.PauseToggle(true);
            UICanvas.Instance.ShowDuckTutorial(()=> 
            {
                isGamePaused = false;
                thirdPersonUserControl.PauseToggle();
                TriggeredEvent(EventTrigger.ALARM_ON);
            });
        }

        public void Dead()
        {
            isGameStarted = false;

            RestartTextUI.SetActive(true);

            thirdPersonUserControl.Dead();
            foreach (Guard g in Guards)
            {
                g.Stop();
            }

            if (ghosts != null)
            {
                foreach (Ghost ghost in ghosts)
                {
                    ghost.StopGhost();
                }
            }

            isRestart = true;
        }

        void Restart()
        {
            isRestart = false;

            RestartTextUI.SetActive(false);

            RestartGame();

            thirdPersonUserControl.PauseToggle();
        }

        public void Replay()
        {
            isReplay = false;

            currentSpawnPointId++;
            if(currentSpawnPointId >= spawnPoints.Length)
            {
                currentSpawnPointId = 0;
            }

            ContinueTextUI.SetActive(false);

            StartGame();

            thirdPersonUserControl.PauseToggle();
        }

		private void Update()
		{
            
#if UNITY_EDITOR
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                Cursor.visible = true;
            }
#endif

            if(isReplay)
            {
//#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
//                if(Input.GetKeyDown("joystick button 16"))
//                {
//                    LoadingScreen.Instance.ShowLoadingScreenToggle(() => 
//                    {
//                        Replay();
//                    }); 
//                }
//#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
//                if (Input.GetKeyDown("joystick button 0"))
//                {
//                    LoadingScreen.Instance.ShowLoadingScreenToggle(() => 
//                    {
//                        Replay();
//                    });                 
//                }
//#endif

                //if (Input.GetKeyDown(KeyCode.E))
                //{
                //    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                //    {
                //        Replay();
                //    });
                //}

                //if (Input.GetMouseButtonDown(0))
                //{
                //    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                //    {
                //        Replay();
                //    });
                //}

                if(Input.anyKeyDown)
                {
                    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                    {
                        Replay();
                    });
                }
            }

            if(isRestart)
            {
//#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
//                if(Input.GetKeyDown("joystick button 16"))
//                {
//                    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
//                    {
//                        Restart();
//                    });
//                }
//#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
//                if (Input.GetKeyDown("joystick button 0"))
//                {
//                    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
//                    {
//                        Restart();
//                    });
//                }
//#endif

                //if (Input.GetKeyDown(KeyCode.E))
                //{
                //    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                //    {
                //        Restart();
                //    });
                //}

                //if (Input.GetMouseButtonDown(0))
                //{
                //    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                //    {
                //        Restart();
                //    });
                //}

                if(Input.anyKeyDown)
                {
                    LoadingScreen.Instance.ShowLoadingScreenToggle(() =>
                    {
                        Restart();
                    });
                }
            }

            if(!isGameStarted)
            {
                return;
            }

            Vector3 direction = TargetPlayer.transform.position - previousPosition;
            Vector3 moveVelocity = TargetPlayer.transform.InverseTransformDirection(direction);
            Vector3 normalisedVelocity = moveVelocity.normalized;

            if(moveVelocity != Vector3.zero)
            {
                if (normalisedVelocity != previousVelocity)
                {
                    PlaybackEvent playbackEvent = new PlaybackEvent();
                    playbackEvent.ActionId = thirdPersonUserControl.Crouch == true ? 1 : 0;
                    playbackEvent.PositionX = TargetPlayer.transform.position.x;
                    playbackEvent.PositionY = 0;
                    playbackEvent.PositionZ = TargetPlayer.transform.position.z;

                    Recorder.Instance.RecordEvent(playbackEvent);
                }

                previousPosition = TargetPlayer.transform.position;
                previousVelocity = normalisedVelocity;
            }

            if (Vector3.Distance(TargetPlayer.transform.position, goal.transform.position) < 1f)
            {
                isGameStarted = false;

                GetNextGoal();
            }
		}

        void GetNextGoal()
        {
            if(currentGoalId > 0)
            {
                currentProgress.ProgressionGoals[currentGoalId-1].UnbindIndicator();
            }

            if (currentProgress != null && currentProgress.ProgressionGoals.Length == currentGoalId)
            {
                progressLevel++;
                currentGoalId = 0;

                if(progressLevel >= Progress.Length)
                {
                    EndDemo();

                    UICanvas.Instance.ShowEndTutorial(() => 
                    {
                        UICanvas.Instance.ShowEpilogue();
                    });
                }
                else
                {
                    EndGame();
                }
            }
            else
            {
                currentProgress = Progress[progressLevel];
                goal = currentProgress.ProgressionGoals[currentGoalId].gameObject;
                goal.SetActive(true);
                currentProgress.ProgressionGoals[currentGoalId].SetProgressText();
                currentProgress.ProgressionGoals[currentGoalId].Initialise();
                currentGoalId++;

                isGameStarted = true;
            }
        }

        void RestartGoal()
        {
            ResetGoals();

            if (currentGoalId > 0)
            {
                currentProgress.ProgressionGoals[currentGoalId - 1].UnbindIndicator();
            }
            else
            {
                currentProgress.ProgressionGoals[currentGoalId].UnbindIndicator();
            }

            currentGoalId = 0;
            goal = currentProgress.ProgressionGoals[currentGoalId].gameObject;
            goal.SetActive(true);
            currentProgress.ProgressionGoals[currentGoalId].SetProgressText();
            currentProgress.ProgressionGoals[currentGoalId].Initialise();
            currentGoalId++;
        }

        void ResetGoals()
        {
            foreach(Progression p in Progress)
            {
                foreach(Goal g in p.ProgressionGoals)
                {
                    g.gameObject.SetActive(false);
                }
            }
        }

        public void TriggeredEvent(EventTrigger eventTrigger)
        {
            switch(eventTrigger)
            {
                case EventTrigger.ALARM_ON:

                    TriggerRecorder(eventTrigger);
                    alarm.TurnOn();

                    if(!isGamePaused)
                    {
                        foreach (Guard g in Guards)
                        {
                            g.Restart();
                        }
                    }
                    
                    break;
                case EventTrigger.ALARM_OFF:
                    TriggerRecorder(eventTrigger);
                    alarm.TurnOff();
                    break;
                case EventTrigger.DEATH:
                    break;
                default:
                    break;
            }
        }

        public void TriggerRecorder(EventTrigger eventTrigger)
        {
            PlaybackEvent playbackEvent = new PlaybackEvent();
            playbackEvent.PositionX = TargetPlayer.transform.position.x;
            playbackEvent.PositionY = 0;
            playbackEvent.PositionZ = TargetPlayer.transform.position.z;
            playbackEvent.EventId = (int)eventTrigger;

            Recorder.Instance.RecordEvent(playbackEvent);
        }

        public void EndGame()
        {
            PlaybackEvent playbackEvent = new PlaybackEvent();
            playbackEvent.PositionX = TargetPlayer.transform.position.x;
            playbackEvent.PositionY = 0;
            playbackEvent.PositionZ = TargetPlayer.transform.position.z;

            Recorder.Instance.RecordEvent(playbackEvent);

            Recorder.Instance.SaveRecords();

            if (ghosts != null)
            {
                foreach (Ghost g in ghosts)
                {
                    g.StopGhost();
                }
            }

            ContinueTextUI.SetActive(true);
            thirdPersonUserControl.PauseToggle(true);
            isReplay = true;
        }

        void EndDemo()
        {
            thirdPersonUserControl.PauseToggle(true);

            if (ghosts != null)
            {
                foreach (Ghost g in ghosts)
                {
                    g.StopGhost();
                }
            }

            foreach (Guard g in Guards)
            {
                g.Stop();
            }
        }

        public void GameOver()
        {
            RestartTextUI.SetActive(true);
        }

        public void ResetAllGameParameters()
        {
            //Reset the alarm;
            alarm.TurnOff();

            //Reset level progression
            progressLevel = 0;
            currentGoalId = 0;
            currentSpawnPointId = 0;

            //Clear the playback records
            playbackRecords.Clear();
            playbackRecords = null;
            Recorder.Instance.ResetRecorder();

            //Remove all ghost from the scene
            if (ghosts != null)
            {
                foreach (Ghost ghost in ghosts)
                {
                    Destroy(ghost.gameObject);
                }

                ghosts.Clear();
            }

            //Start main screen sequence
            BackgroundMusic.Instance.MainScreenBGM();
            MainScreen.SetActive(true);
        }
	}   
}