﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Story
{
    [Serializable]
    public class Dialog
    {
        public int DialogPointId;
        public string DialogText;
    }

    [Serializable]
    public class DialogList
    {
        public List<Dialog> Dialogs;
    }
}