﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Playback
{
    public class Recorder : MonoBehaviour
    {
        float startTime;
        int playbackIndex = 0;
        int highestRecordIdNumber = 0;
        Record newRecord;
        List<Record> recordsList;

        public static Recorder Instance
        {
            private set;
            get;   
        }

		private void Awake()
		{
            Instance = this;
		}

        /// <summary>
        /// Prepares the recorder.
        /// </summary>
        public List<Record> StartRecorder(List<Record> records = null)
        {
            if(records == null)
            {
                recordsList = new List<Record>();
            }
            else
            {
                recordsList = records;
            }

            newRecord = new Record();
            newRecord.RecordId = GetNewRecordId();

            startTime = Time.time; //Marks recorder start time.

            return recordsList;
        }

        /// <summary>
        /// Resets the recorder.
        /// </summary>
        public void ResetRecorder()
        {
            playbackIndex = 0;
        }

        /// <summary>
        /// Erase all playback events and restart the recorder.
        /// </summary>
        public void RestartRecorder()
        {
            ResetRecorder();

            newRecord.PlaybackEvents.Clear();
        }

        /// <summary>
        /// Records triggered event.
        /// </summary>
        /// <param name="playbackEvent">Playback event.</param>
        public void RecordEvent(PlaybackEvent playbackEvent)
        {
            playbackEvent.PlaybackIndex = playbackIndex;

            float elapsedTime = Time.time - startTime; //Time elapsed since the recorder started.

            playbackEvent.Time = elapsedTime;//Records the current playback time

            if (newRecord.PlaybackEvents == null)
                newRecord.PlaybackEvents = new List<PlaybackEvent>();
            
            newRecord.PlaybackEvents.Add(playbackEvent);

            playbackIndex++;
            startTime = Time.time;
        }

        /// <summary>
        /// Saves all recorded playback to JSON file.
        /// </summary>
        public List<Record> SaveRecords()
        {
            if(recordsList!=null)
            {
                recordsList.Add(newRecord);

                //string recordJSON = JsonUtility.ToJson(recordsList);
            }

            ResetRecorder();

            return recordsList;
        }

        /// <summary>
        /// Gets the new record identifier.
        /// </summary>
        /// <returns>The new record identifier.</returns>
        Int64 GetNewRecordId()
        {
            Int64 newHighestId = 0;

            foreach(Record r in recordsList)
            {
                if(r.RecordId > newHighestId)
                {
                    newHighestId = r.RecordId;
                }
            }

            return newHighestId + 1;
        }
	}   
}