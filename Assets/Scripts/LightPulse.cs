﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Lights
{
    public class LightPulse : MonoBehaviour
    {
        [SerializeField]
        float speed;

        [SerializeField]
        float maxIntensity;

        Light light;

        void Awake()
        {
            light = GetComponent<Light>();
        }

        void Update()
        {
            if (light == null) return;

            light.intensity = Mathf.PingPong(Time.time * speed, maxIntensity);
        }
    }   
}