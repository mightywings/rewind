﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind.Core.Environment;
using Rewind.Core.Manager;

namespace Rewind.GUI
{
    public class MainScreen : MonoBehaviour
    {
        [SerializeField]
        GameObject parentObject;

		private void Update()
        {
            if(Input.anyKeyDown)
            {
                GameManager.Instance.BeginGameSequence();
                parentObject.SetActive(false);
                gameObject.SetActive(false);
                BackgroundMusic.Instance.GameScreenBGM();
            }
        }
    
    }
}