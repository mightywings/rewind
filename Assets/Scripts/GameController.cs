﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Controller
{
    [ExecuteInEditMode]
    public class GameController : MonoBehaviour
    {
		private void Update()
		{
            for (int i = 0; i < 20; i++)
            {
                if (Input.GetKeyDown("joystick button " + i))
                {
                    Debug.Log("Button " + i + " is pressed.");
                }
            }
		}
	}   
}