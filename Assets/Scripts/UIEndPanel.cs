﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rewind.GUI
{
    public class UIEndPanel : MonoBehaviour
    {
        private UnityAction onCrouchPressed;

        public void ShowUI(UnityAction crouchPressed)
        {
            onCrouchPressed = crouchPressed;
        }

        private void Update()
        {
            if (Input.anyKeyDown)
            {
                onCrouchPressed();
                gameObject.SetActive(false);
            }
        }
    }
}