﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Rewind.GUI;
using Rewind.GUI.Story;

namespace Rewind.Core.SpawnPoint
{
    public class StartPoint : MonoBehaviour
    {
        public string NarrativeText;
        public string DateYearText;
        public Transform UIAnchorPoint;
        public Canvas WorldCanvas;

        NarrativePanel narrativePanel;

        private void Start()
        {
            narrativePanel = UICanvas.Instance.NarrativePanel;
        }

        public void ShowWorldCanvas()
        {
            WorldCanvas.transform.SetParent(UIAnchorPoint);
            WorldCanvas.transform.localPosition = Vector3.zero;
            WorldCanvas.transform.localEulerAngles = Vector3.zero;
            UIWorld uIWorld = WorldCanvas.GetComponent<UIWorld>();
            uIWorld.DateText.text = DateYearText;
            uIWorld.Restart();
        }

        public void ShowWorldCanvasRestart()
        {
            WorldCanvas.transform.SetParent(UIAnchorPoint);
            WorldCanvas.transform.localPosition = Vector3.zero;
            WorldCanvas.transform.localEulerAngles = Vector3.zero;
            UIWorld uIWorld = WorldCanvas.GetComponent<UIWorld>();
            uIWorld.DateText.text = DateYearText;
            uIWorld.ResetUI();
        }

		private void OnTriggerEnter(Collider other)
		{
            if (!other.tag.Equals("Player"))
            {
                return;
            }

            StartCoroutine(Delay(()=>
            {
                narrativePanel.SetText(NarrativeText);
            }));
		}

		private void OnTriggerExit(Collider other)
		{
            if (!other.tag.Equals("Player"))
            {
                return;
            }

            //narrativePanel.NarrativePanelDisplayToggle(false);

            gameObject.SetActive(false);
		}

        IEnumerator Delay(UnityAction onCompleted)
        {
            yield return new WaitForSeconds(1);
            onCompleted();
        }
	}   
}