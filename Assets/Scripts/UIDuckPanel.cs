﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rewind.GUI
{
    public class UIDuckPanel : MonoBehaviour
    {
        private UnityAction onCrouchPressed;

        public void ShowUI(UnityAction crouchPressed)
        {
            onCrouchPressed = crouchPressed;
        }

        private void Update()
        {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
                if(Input.GetKeyDown("joystick button 17"))
                {
                    onCrouchPressed();
                    gameObject.SetActive(false);
                }
#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (Input.GetKeyDown("joystick button 1"))
            {
                onCrouchPressed();
                gameObject.SetActive(false);
            }
#endif

            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                onCrouchPressed();
                gameObject.SetActive(false);
            }
        }
    }
}