﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind.Core.Manager;

namespace Rewind.GUI
{
    public class EpilogueUI : MonoBehaviour
    {
        public bool ResetTheGame = true;

        public void EndGame()
        {
            if (ResetTheGame)
            {
                GameManager.Instance.ResetAllGameParameters();
                gameObject.SetActive(false);
            }
            else
            {

#if UNITY_EDITOR
                //UnityEditor.EditorApplication.isPlaying = false;

#else
                Application.Quit();
#endif
            }
        }
    }   
}