﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rewind.GUI
{
    public class LoadingScreen : MonoBehaviour
    {
        public static LoadingScreen Instance
        {
            get;
            private set;
        }

        [SerializeField]
        private GameObject loadingScreenPanel;

        [SerializeField]
        private float loadDuration = 5.52f;

        private void Awake()
        {
            Instance = this;
        }

        public void ShowLoadingScreenToggle(UnityAction onComplete)
        {
            loadingScreenPanel.SetActive(true);
            StartCoroutine(DelayLoad(onComplete));
        }

        IEnumerator DelayLoad(UnityAction onComplete)
        {
            yield return new WaitForSeconds(loadDuration);
            onComplete();
            loadingScreenPanel.SetActive(false);
        }
    }
}