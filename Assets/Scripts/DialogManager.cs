﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind.Core.Story;

namespace Rewind.Core.Manager
{
    public class DialogManager : MonoBehaviour
    {
        [SerializeField]
        TextAsset dialogConfigs;

        [SerializeField]
        DialogPoint[] dialogPoints;

        DialogList dialogList;

		private void Start()
        {
            dialogList = JsonUtility.FromJson<DialogList>(dialogConfigs.text);
            foreach(DialogPoint d in dialogPoints)
            {
                d.SetNarrativeText(GetDialog(d.id));
            }
		}

        public string GetDialog(int dialogId)
        {
            var result = dialogList.Dialogs.Where(d => d.DialogPointId == dialogId);
            if(result.Count() > 0)
            {
                Dialog dialog = result.First();
                return dialog.DialogText;
            }

            return string.Empty;
        }

        public void ResetDialog()
        {
            foreach (DialogPoint d in dialogPoints)
            {
                d.ResetNarrative();
            }
        }
	}   
}