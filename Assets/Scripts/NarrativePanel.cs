﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Rewind.GUI.Story
{
    public class NarrativePanel : MonoBehaviour
    {
        [SerializeField]
        float fadeSpeed = 0.1f;

        [SerializeField]
        bool useFade = true;

        Text narrativeTextBox;
        CanvasGroup narrativePanelGroup;

		private void Awake()
		{
            narrativeTextBox = GetComponentInChildren<Text>();
            narrativePanelGroup = GetComponent<CanvasGroup>();
            narrativePanelGroup.alpha = 0;
		}

        public void SetText(string sourceText)
        {
            if (narrativeTextBox != null)
            {
                narrativeTextBox.text = sourceText;
                NarrativePanelDisplayToggle(true);
            }
        }

        public void NarrativePanelDisplayToggle(bool isShow)
        {
            if (useFade)
            {
                StartCoroutine(Fade(isShow));
            }
            else
            {
                if (isShow)
                {
                    narrativePanelGroup.alpha = 1;
                }
                else
                {
                    narrativePanelGroup.alpha = 0;
                }
            }
        }

        public void ResetNarrativePanel()
        {
            narrativePanelGroup.alpha = 0;
        }

        IEnumerator Fade(bool isShow)
        {
            if (isShow)
            {
                while (narrativePanelGroup.alpha < 1)
                {
                    narrativePanelGroup.alpha += 0.1f;
                    yield return new WaitForSeconds(fadeSpeed);
                }

                yield return new WaitForEndOfFrame();

                narrativePanelGroup.alpha = 1;
            }
            else
            {
                while (narrativePanelGroup.alpha > 0)
                {
                    narrativePanelGroup.alpha -= 0.1f;
                    yield return new WaitForSeconds(fadeSpeed);
                }

                yield return new WaitForEndOfFrame();

                narrativePanelGroup.alpha = 0;
            }
        }
	}   
}