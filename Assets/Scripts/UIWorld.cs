﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace Rewind.GUI
{
    public class UIWorld : MonoBehaviour
    {
        public Text TimeText;
        public Text DateText;
        public Text TimelineText;

        public bool TrackCamera = false;

        private Transform cameraTransform;
        private DateTime wantedDateTime;
        private float startTime;
        private int timelineCounter = 0;

		private void Start()
		{
            cameraTransform = Camera.main.transform;
            wantedDateTime = new DateTime(1980, 1, 1, 23, 0, 0);
            startTime = Time.time;
            TimelineText.text = "Timeline " + timelineCounter;
		}

        public void Restart()
        {
            timelineCounter++;
            TimelineText.text = "Timeline " + timelineCounter;
            wantedDateTime = new DateTime(1980, 1, 1, 23, 0, 0);
            startTime = Time.time;
        }

        public void ResetUI()
        {
            TimelineText.text = "Timeline " + timelineCounter;
            wantedDateTime = new DateTime(1980, 1, 1, 23, 0, 0);
            startTime = Time.time;
        }

		private void Update()
		{
            if (cameraTransform == null) return;

            if(TrackCamera)
            {
                transform.LookAt(cameraTransform);
            }

            if(Time.time - startTime >= 1)
            {
                wantedDateTime = wantedDateTime.AddSeconds(1);
                startTime = Time.time;
            }

            TimeText.text = wantedDateTime.ToLongTimeString();
		}
	}   
}