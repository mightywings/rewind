﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind.GUI;

namespace Rewind.Core
{
    public class Goal : MonoBehaviour
    {
        [SerializeField]
        Color indicatorColour;

        [SerializeField]
        string objectId;

        public string GoalText;

        public void Initialise()
        {
            UICanvas.Instance.CreateIndicator(objectId,transform, indicatorColour,true);
        }

        public void UnbindIndicator()
        {
            UICanvas.Instance.RemoveIndicator(objectId);
        }

        public void SetProgressText()
        {
            UICanvas.Instance.SetGoalProgressText(int.Parse(objectId), GoalText);
        }
    }   
}