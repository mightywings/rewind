﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Environment
{
    public class Alarm : MonoBehaviour
    {
        public GameObject alarmLight;
        public AudioSource audioSource;
        public Light AlarmTerminalLight;
        public Color AlarmOn;
        public Color AlarmOff;

        public void TurnOn()
        {
            alarmLight.SetActive(true);
            audioSource.enabled = true;
            audioSource.Play();

            AlarmTerminalLight.color = AlarmOn;
        }

        public void TurnOff()
        {
            alarmLight.SetActive(false);
            audioSource.enabled = false;

            AlarmTerminalLight.color = AlarmOff;
        }
    }
}