﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.GUI
{
    public class MainScreenOpening : MonoBehaviour
    {
        [SerializeField]
        GameObject pressAnyKeyUI;

        public void ShowUI()
        {
            pressAnyKeyUI.SetActive(true);
        }
    }

}