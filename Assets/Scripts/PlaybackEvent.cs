﻿using System;

namespace Rewind.Core.Playback
{
    [Serializable]
    public struct PlaybackEvent
    {
        public Int64 PlaybackIndex { get; set; }
        public float Time { get; set; }
        public Int64 ActionId { get; set; }
        public Int64 EventId { get; set; }
        public float PositionX { get; set; }
        public float PositionY { get; set; }
        public float PositionZ { get; set; }
    }   

    public enum EventTrigger
    {
        NONE,
        ALARM_ON,
        ALARM_OFF,
        DEATH,
        PICKUP_ITEM,
        REMOVE_ITEM,
        SHOW_TUTORIAL,
    }
}