﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using Rewind.Core.Playback;
using Rewind.GUI;
using Rewind.Core.Manager;
using UnityStandardAssets.Characters.ThirdPerson;
using Rewind.Core.Environment;

namespace Rewind.Core.Character
{
    public class Ghost : MonoBehaviour
    {
        [SerializeField]
        NavMeshAgent localNavMeshAgent;

        [SerializeField]
        float distanceToNoticePlayer = 5.0f;

        [SerializeField]
        GameObject ghostMesh;

        [SerializeField]
        GameObject ghostPixels;

        public Color IndicatoreColour;
        public Transform indicatorAnchor;

        Record localRecord;
        PlaybackEvent currentEvent;
        int currentPlaybackId;
        bool isPlayback = false;
        Animator characterAnimator;
        Transform playerToTrack;

		private void Start()
		{
            characterAnimator = GetComponent<Animator>();
		}

		public void InitialiseGhost(Record record,string id,Transform targetPlayer)
        {
            localRecord = record;
            playerToTrack = targetPlayer;
            UICanvas.Instance.CreateIndicator("Ghost" + id, indicatorAnchor, IndicatoreColour, false,true);
            GetFirstPlaybackEvent();
        }

        void GetFirstPlaybackEvent()
        {
            var result = localRecord.PlaybackEvents.Where(p => p.PlaybackIndex == 0);
            if(result.Count() > 0)
            {
                currentEvent = result.First();
                localNavMeshAgent.Warp(new Vector3(currentEvent.PositionX, currentEvent.PositionY, currentEvent.PositionZ));;

                isPlayback = true;
            }
        }

        void HideGhost()
        {
            localNavMeshAgent.enabled = false;
            isPlayback = false;

            ghostMesh.SetActive(false);
        }

        public void StopGhost()
        {
            localNavMeshAgent.enabled = false;
            isPlayback = false;

            ghostMesh.SetActive(false);
            ghostPixels.SetActive(true);
        }

		void Update()
		{
            if(isPlayback)
            {
                if(localNavMeshAgent.enabled == true)
                {
                    transform.position = new Vector3(transform.position.x, 0, transform.position.z);

                    Vector3 target = new Vector3(currentEvent.PositionX, currentEvent.PositionY, currentEvent.PositionZ);
                    localNavMeshAgent.destination = target;
                    float axisVelocity = 0;
                    if(Mathf.Abs(localNavMeshAgent.velocity.x) > axisVelocity)
                    {
                        axisVelocity = Mathf.Abs(localNavMeshAgent.velocity.x);
                    }

                    if (Mathf.Abs(localNavMeshAgent.velocity.z) > axisVelocity)
                    {
                        axisVelocity = Mathf.Abs(localNavMeshAgent.velocity.z);
                    }

                    characterAnimator.SetFloat("Forward", axisVelocity,0.1f, Time.deltaTime);
                    characterAnimator.SetBool("Crouch", currentEvent.ActionId==1?true:false);


                    if (localNavMeshAgent.remainingDistance < 1f)
                    {
                        GetNextEvent();
                    }

                    TrackPlayer();
                }
            }
		}

        void TrackPlayer()
        {
            if(Vector3.Distance(transform.position,playerToTrack.position) < distanceToNoticePlayer)
            {
                GameManager.Instance.Dead();

                StopGhost();


            }
        }

        void GetNextEvent()
        {
            currentPlaybackId++;
            var newPlaybackEvent = localRecord.PlaybackEvents.Where(p => p.PlaybackIndex == currentPlaybackId);
            if (newPlaybackEvent.Count() > 0)
            {
                EventTrigger eventTrigger = (EventTrigger)currentEvent.EventId;
                if(eventTrigger != EventTrigger.NONE)
                {
                    GameManager.Instance.TriggeredEvent(eventTrigger);
                }

                currentEvent = newPlaybackEvent.First();
                isPlayback = true;
            }
            else
            {
                //StopGhost();
                HideGhost();
            }
        }

        IEnumerator PlaybackDelay(float duration)
        {
            yield return new WaitForSeconds(duration);
            GetNextEvent();
        }

        IEnumerator Delay(UnityAction onComplete)
        {
            yield return new WaitForSeconds(2f);
            onComplete();
        }

		private void OnDestroy()
		{
            localRecord = null;
		}
	}   
}