﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Rewind.GUI.Indicator;
using Rewind.GUI.Story;

namespace Rewind.GUI
{
    public class UICanvas : MonoBehaviour
    {
        public Camera TargetCamera;
        public Transform PlayerObject;

        [SerializeField]
        GameObject indicatorTemplate;

        [SerializeField]
        NarrativePanel narrativePanel;

        public NarrativePanel NarrativePanel
        {
            get { return narrativePanel; }
        }

        [SerializeField]
        UIGoal uIGoal;

        [SerializeField]
        GameObject almanacUI;

        [SerializeField]
        UIDuckPanel duckTutorialUI;

        [SerializeField]
        UIWelcomePanel welcomePanelUI;

        [SerializeField]
        UIEndPanel endPanelUI;

        [SerializeField]
        GameObject epilogue;

        Dictionary<string,UIIndicator> indicators = new Dictionary<string, UIIndicator>();

        static UICanvas instance;
        public static UICanvas Instance
        {
            get { return instance; }
        }

		private void Awake()
		{
            instance = this;
		}

        public void CreateIndicator(string objectId, Transform target,
                                    Color targetColor, bool showGoalNumber, bool autoScale = false)
        {
            indicatorTemplate.SetActive(true);
            GameObject indicatorObject = Instantiate(indicatorTemplate);
            UIIndicator ui = indicatorObject.GetComponent<UIIndicator>();
            ui.Initialise(objectId,GetComponent<RectTransform>(), target, 
                          TargetCamera, PlayerObject,targetColor,showGoalNumber,autoScale);

            indicators.Add(objectId, ui);

            indicatorTemplate.SetActive(false);
        }

        public void ResetIndicator()
        {
            foreach(KeyValuePair<string,UIIndicator> kvp in indicators)
            {
                Destroy(kvp.Value.gameObject);
            }

            indicators.Clear();
        }

        public void RemoveIndicator(string objectId)
        {
            if(indicators.ContainsKey(objectId))
            {
                Destroy(indicators[objectId].gameObject);
                indicators.Remove(objectId);
            }
        }

        public void SetGoalProgressText(int goalId, string goalString)
        {
            uIGoal.SetGoalTextAndId(goalId, goalString);
        }

        public void ShowAlmanacToggle(bool isActive = false)
        {
            almanacUI.SetActive(isActive);
        }

        public void ShowDuckTutorial(UnityAction onComplete)
        {
            duckTutorialUI.gameObject.SetActive(true);
            duckTutorialUI.ShowUI(onComplete);
        }

        public void ShowWelcomeTutorial(UnityAction onComplete)
        {
            welcomePanelUI.gameObject.SetActive(true);
            welcomePanelUI.ShowUI(onComplete);
        }

        public void ShowEndTutorial(UnityAction onComplete)
        {
            endPanelUI.gameObject.SetActive(true);
            endPanelUI.ShowUI(onComplete);
        }

        public void ShowEpilogue()
        {
            epilogue.SetActive(true);
        }
    }   
}