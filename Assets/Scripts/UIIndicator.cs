﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Rewind.GUI.Indicator
{
    public class UIIndicator : MonoBehaviour
    {
        [SerializeField]
        Text distanceText;

        [SerializeField]
        Text goalNumberText;

        [SerializeField]
        Text targetName;

        [SerializeField]
        RectTransform indicatorTransform;

        [SerializeField]
        float viewAngle = 90f;

        [SerializeField]
        Image indicatorTexture;

        [SerializeField]
        Image borderTexture;

        [SerializeField]
        float maxDistance;

        [SerializeField]
        float minScaleValue;

        RectTransform parentCanvas;
        Transform objectToTrack;
        Camera uiCamera;
        Transform playerObject;
        bool isReady = false;
        bool scaleWithDistance;

        public virtual void Initialise(string goalId, RectTransform parent, Transform target,
                                       Camera camera, Transform player, Color targetColor, 
                                       bool showGoalNumber, bool autoScale = false)
        {
            parentCanvas = parent;
            objectToTrack = target;
            uiCamera = camera;
            playerObject = player;
            scaleWithDistance = autoScale;

            indicatorTexture.color = targetColor;

            transform.SetParent(parentCanvas);

            goalNumberText.text = goalId;

            if(goalId.Contains("Ghost0"))
            {
                targetName.text = "Marty A";
            }
            else if(goalId.Contains("Ghost1"))
            {
                targetName.text = "Marty B";
            }
            else if (goalId.Contains("Guard1"))
            {
                targetName.text = "Sentry Guard";
            }
            else
            {
                targetName.gameObject.SetActive(false);
            }

            if (showGoalNumber)
            {
                goalNumberText.gameObject.SetActive(true);
            }


            isReady = true;
        }

        public void SetBorderColour(Color targetBorderColor)
        {
            borderTexture.color = targetBorderColor;
        }

		protected virtual void Update()
		{
            if (!isReady) return;

            if (playerObject == null || objectToTrack == null) return;

            float distanceToTarget = Vector3.Distance(playerObject.position, objectToTrack.position);
            distanceText.text = ((int)distanceToTarget).ToString() + " m";

            if(scaleWithDistance)
            {
                float distanceDelta = maxDistance - distanceToTarget;
                float targetScale = distanceDelta / maxDistance;
                if(targetScale < minScaleValue)
                {
                    targetScale = minScaleValue;
                }

                borderTexture.GetComponent<RectTransform>().localScale = new Vector3(targetScale, targetScale, targetScale);
            }

            Vector2 indicatorPosition = RectTransformUtility.WorldToScreenPoint(uiCamera, objectToTrack.position);
            Vector2 screenPoint;

            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas, indicatorPosition, null, out screenPoint);

            float y = screenPoint.y;
            if (Mathf.Abs(y) > (300))
            {
                y = -(200);
            }

            if (screenPoint.x < -Screen.width/2)
            {
                screenPoint = new Vector2(-(380) , y);
            }

            if (screenPoint.x > Screen.width/2)
            {
                screenPoint = new Vector2(380, y);
            }

            if (IsTargetBehindCamera())
            {
                screenPoint = new Vector2(-screenPoint.x, -200);
            }

            indicatorTransform.localPosition = screenPoint;
		}

        bool IsTargetBehindCamera()
        {
            Vector3 vectorToTarget = objectToTrack.position - uiCamera.transform.position;
            if (Vector3.Angle(vectorToTarget, uiCamera.transform.forward) > viewAngle)
            {
                return true;
            }
            return false;
        }
	}   
}