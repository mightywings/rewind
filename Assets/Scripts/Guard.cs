﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Rewind.GUI;
using Rewind.Core.Manager;

namespace Rewind.Core.Character
{
    public class Guard : MonoBehaviour
    {
        [SerializeField]
        string guardId;

        public Transform[] Waypoints;
        public NavMeshAgent GuardAgent;
        public Transform RayOutputPoint;
        public float ActionStateInterval;
        public float AfterAlertInterval;
        public float ScanSpeed = 1f;
        public float RayDistance;
        public float AttackDistance;
        public Color IndicatoreColour;
        public Transform indicatorAnchor;
        public int StartWaypointIndex = 0;
        public LineRenderer laserRenderer;
        public Transform StandbyPosition;

        float startTime;
        bool isReady = false;
        ActionState actionState;
        int currentWaypointIndex = 0;
        RaycastHit hit;

        public enum ActionState
        {
            STANDBY,
            PATROL,
            ALERT,
            ATTACK,
            NONE,
        }

        private void Start()
        {
            currentWaypointIndex = StartWaypointIndex;
            GuardAgent.Warp(Waypoints[currentWaypointIndex].position);
        }

        public void ExecutePatrol()
        {
            UICanvas.Instance.RemoveIndicator(guardId);
            UICanvas.Instance.CreateIndicator(guardId,indicatorAnchor, IndicatoreColour,false,true);

            //if(GuardAgent != null)
            //{
            //    currentWaypointIndex = StartWaypointIndex;
            //    GuardAgent.Warp(Waypoints[currentWaypointIndex].position);
            //}

            startTime = Time.time;
            actionState = ActionState.STANDBY;
            isReady = true;
            currentWaypointIndex++;
        }

        public void Stop()
        {
            isReady = false;
        }

		private void Update()
		{
            if(!isReady)
            {
                return;
            }

            if(actionState == ActionState.STANDBY)
            {
                if (Time.time - startTime > ActionStateInterval)
                {
                    actionState = ActionState.PATROL;
                    GuardAgent.destination = Waypoints[currentWaypointIndex].position;
                }
                else
                {
                    //Scan for intruder
                    if (ScanForIntruder())
                    {
                        actionState = ActionState.ATTACK;
                    }
                    transform.Rotate(Vector3.up * Time.deltaTime * ScanSpeed, Space.World);
                }
            }
            else if(actionState == ActionState.PATROL)
            {
                if(GuardAgent.remainingDistance < 0.3f)
                {
                    actionState = ActionState.STANDBY;
                    startTime = Time.time;
                    currentWaypointIndex++;
                    if(currentWaypointIndex >= Waypoints.Length)
                    {
                        currentWaypointIndex = 0;
                    }
                }
                else
                {
                    //Scan for intruder
                    if (ScanForIntruder())
                    {
                        actionState = ActionState.ATTACK;
                    }
                }
            }
            else if(actionState == ActionState.ATTACK)
            {
                laserRenderer.startColor = Color.red;
                laserRenderer.endColor = Color.red;
                laserRenderer.SetPosition(1, new Vector3(0, 1, hit.distance));
                Debug.DrawRay(RayOutputPoint.position, RayOutputPoint.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            }
		}

        bool ScanForIntruder()
        {
            if (Physics.Raycast(RayOutputPoint.position, RayOutputPoint.TransformDirection(Vector3.forward), out hit, RayDistance))
            {
                if(hit.collider.tag == "Player")
                {
                    GameManager.Instance.Dead();
                    return true;
                }
                else
                {
                    laserRenderer.startColor = Color.red;
                    laserRenderer.endColor = Color.red;
                    laserRenderer.SetPosition(1, new Vector3(0,1,hit.distance));
                    Debug.DrawRay(RayOutputPoint.position, RayOutputPoint.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    return false;
                }
            }
            else
            {
                laserRenderer.startColor = Color.red;
                laserRenderer.endColor = Color.red;
                laserRenderer.SetPosition(1, new Vector3(0, 1, RayDistance));
                Debug.DrawRay(RayOutputPoint.position, RayOutputPoint.TransformDirection(Vector3.forward) * RayDistance, Color.white);
            }

            return false;
        }

        public void Reposition()
        {
            currentWaypointIndex = StartWaypointIndex;
            GuardAgent.Warp(Waypoints[currentWaypointIndex].position);
            actionState = ActionState.NONE;
            //transform.position = StandbyPosition.position;
        }

        public void Restart()
        {
            currentWaypointIndex = StartWaypointIndex;
            GuardAgent.Warp(Waypoints[currentWaypointIndex].position);
            ExecutePatrol();
        }
	}   
}