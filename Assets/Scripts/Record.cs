using System;
using System.Collections.Generic;

namespace Rewind.Core.Playback
{
    [Serializable]
    public class Record
    {
        public Int64 RecordId { get; set; }
        public List<PlaybackEvent> PlaybackEvents {get;set;}
    }   
}