﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewind.GUI;
using Rewind.GUI.Story;
using Rewind.Core.Manager;
using Rewind.Core.Playback;

namespace Rewind.Core.Story
{
    public class DialogPoint : MonoBehaviour
    {
        public int id;
        public bool useOnce = true;

        public EventTrigger TriggerType = EventTrigger.NONE;
        public EventTrigger SecondaryTrigger = EventTrigger.NONE;
        public GameObject PickUpObject;
        public Light AlarmTerminalLight;
        public Color AlarmOn;
        public Color AlarmOff;

        NarrativePanel narrativePanel;
        string narrativeText;
        bool isShown = false;

		private void Start()
		{
            narrativePanel = UICanvas.Instance.NarrativePanel;
		}

		public void SetNarrativeText(string sourceText)
        {
            narrativeText = sourceText;
        }

        public void ResetNarrative()
        {
            isShown = false;
        }

		private void OnTriggerEnter(Collider other)
		{
            if (isShown) return;

            if(!other.tag.Equals("Player"))
            {
                return;
            }

            if(SecondaryTrigger == EventTrigger.PICKUP_ITEM)
            {
                PickUpObject.SetActive(false);
                UICanvas.Instance.ShowAlmanacToggle(true);
            }
            else if(SecondaryTrigger == EventTrigger.REMOVE_ITEM)
            {
                PickUpObject.SetActive(false);
            }
            else if (SecondaryTrigger == EventTrigger.SHOW_TUTORIAL)
            {
                GameManager.Instance.PauseGame();
            }

            if (TriggerType != EventTrigger.NONE)
            {
                if(TriggerType == EventTrigger.ALARM_OFF)
                {
                    AlarmTerminalLight.color = AlarmOff;
                }
                else if(TriggerType == EventTrigger.ALARM_ON)
                {
                    AlarmTerminalLight.color = AlarmOn;
                }

                GameManager.Instance.TriggeredEvent(TriggerType);
            }

            narrativePanel.SetText(narrativeText);
		}

		private void OnTriggerExit(Collider other)
		{
            if (!other.tag.Equals("Player"))
            {
                return;
            }

            if(useOnce)
            {
                isShown = true;
            }

            gameObject.SetActive(false);
		}
	}

}