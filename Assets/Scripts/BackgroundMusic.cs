﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rewind.Core.Environment
{
    public class BackgroundMusic : MonoBehaviour
    {
        public AudioClip MainscreenBGMClip;
        public AudioClip GameBGMClip;
        public AudioSource MainAudioSource;

        public static BackgroundMusic Instance
        {
            get;
            private set;
        }

        private void Awake()
        {
            Instance = this;
        }

        public void MainScreenBGM()
        {
            MainAudioSource.clip = MainscreenBGMClip;
            MainAudioSource.enabled = true;
            MainAudioSource.Play();
        }

        public void GameScreenBGM()
        {
            MainAudioSource.clip = GameBGMClip;
            MainAudioSource.enabled = true;
            MainAudioSource.Play();
        }
    }
}