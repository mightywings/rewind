﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Rewind.GUI
{
    public class UIGoal : MonoBehaviour
    {
        [SerializeField]
        Text goalNumber;

        [SerializeField]
        Text goalText;

        public void SetGoalTextAndId(int goalId, string goalString)
        {
            goalNumber.text = goalId.ToString();
            goalText.text = goalString;
        }
    }   
}